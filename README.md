## Omnidirecitonal shadows and PBR-shading
========================

## To build:
* make dir build
* step into build
* cmake ..
* make pbrMdlInspector

## To run:
* unzip bin/gfx/textures/textures.zip in the folder it is located in.
* unzip bin/zombieWalk/zombieWalk.zip in the folder it is located in
* step to the bin folder and run pbrMdlInspector
