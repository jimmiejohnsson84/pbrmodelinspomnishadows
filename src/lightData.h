#ifndef LIGHT_DATA_H
#define LIGHT_DATA_H


class SceneLightData
{
	public:
	glm::vec3	m_lightPos;
	glm::vec3	m_lightColor;
	float		m_lightIntensity;
};

void	InitLightData(SceneLightData &sceneLightData);

#endif