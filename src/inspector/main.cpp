// Include standard headers
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <vector>
#include <sstream>

#if defined(_WIN32)
	#include <time.h>
    #include <string>	
#endif

using namespace std;

// Include GLEW
#include <GL/glew.h>

// Include GLFW
#include <GLFW/glfw3.h>

// Include GLM
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
using namespace glm;


#include "../shader.h"
#include "../initGL.h"
#include "../model3d_cooktorrance.h"
#include "../texture.h"
#include "../lightData.h"
#include "../renderStruct.h"
#include "../text2D.h"
#include "../texturedQuad.h"
#include "../animPlayer.h"

void DisplayStartupText()
{
	std::string line;
	ifstream startUpText ("pBRModelInspector_startupText.txt");
	if (startUpText.is_open())
	{
		while (getline (startUpText, line) )
		{
			cout << line << endl;
		}
		startUpText.close();
	}	
}

void TextControls(std::vector<Text2D*> &textControls)
{
	#if defined(_WIN32)
		const float yStart = 0.0f;
		const float textOffsetY = 19.0f;
		const float fontSize = 16.0f;
	#else
		const float yStart = 70.0f;
		const float textOffsetY = 17.0f;
		const float fontSize = 15.0f;
	#endif
	
	textControls.push_back(new Text2D("ESC - QUIT", glm::vec3(20.0f, 													yStart + textOffsetY * 1.0f, 0.0f), fontSize));
	textControls.push_back(new Text2D("PAGEUP,PAGEDOWN - MOVE MODEL BACKWARD, FORWARD", glm::vec3(20.0f, 				yStart + textOffsetY * 2.0f, 0.0f), fontSize));
	textControls.push_back(new Text2D("W,S,A,D,E,R - ROTATES MODEL", glm::vec3(20.0f, 									yStart + textOffsetY * 3.0f, 0.0f), fontSize));
	textControls.push_back(new Text2D("UP,DOWN,LEFT,RIGHT - MOVES MODEL", glm::vec3(20.0f, 								yStart + textOffsetY * 4.0f, 0.0f), fontSize));
	textControls.push_back(new Text2D("SPACE - SWITCH MODEL", glm::vec3(20.0f, 											yStart + textOffsetY * 5.0f, 0.0f), fontSize));
	textControls.push_back(new Text2D("MOUSE - MOVE LIGHT", glm::vec3(20.0f, 											yStart + textOffsetY * 6.0f, 0.0f), fontSize));
	textControls.push_back(new Text2D("SHIFT + MOUSE - MOVE LIGHT FORWARD,BACKWARD", glm::vec3(20.0f, 					yStart + textOffsetY * 7.0f, 0.0f), fontSize));
	textControls.push_back(new Text2D("LEFT MOUSE BUTTON - CHANGE LIGHT COLOR", glm::vec3(20.0f, 						yStart + textOffsetY * 8.0f, 0.0f), fontSize));
	textControls.push_back(new Text2D("+,- CHANGES LIGHT BRIGHTNESS", glm::vec3(20.0f, 									yStart + textOffsetY * 9.0f, 0.0f), fontSize));
}

void HandleInput(GLFWwindow* window, bool &useModel1, int &texturePackage, bool &spaceRel, bool &shiftDown,	bool &mouseLeftButtonRel,
					glm::vec3 &modelPos, glm::vec3 &modelRot, SceneLightData &sceneLight, int &colorLightIndex, float deltaTime)
{
	const float rotSpeed = 60.0f;
	const float moveSpeed = 2.0f;

    #ifdef WIN32
	    const float mouseSpeed  = 1.0f;
    #else
        const float mouseSpeed = 1.0f;
    #endif

	const float lightIntensityChangeSpeed = 25.0f;

	// Move
	if (glfwGetKey(window, GLFW_KEY_UP ) == GLFW_PRESS)
	{
		modelPos.y += deltaTime * moveSpeed;			
	}	

	if (glfwGetKey(window, GLFW_KEY_DOWN ) == GLFW_PRESS)
	{
		modelPos.y -= deltaTime * moveSpeed;		
	}	

	if (glfwGetKey(window, GLFW_KEY_LEFT ) == GLFW_PRESS)
	{
		modelPos.x -= deltaTime * moveSpeed;		
	}
	
	if (glfwGetKey(window, GLFW_KEY_RIGHT ) == GLFW_PRESS)
	{
		modelPos.x += deltaTime * moveSpeed;
	}

	if (glfwGetKey(window, GLFW_KEY_PAGE_UP ) == GLFW_PRESS)
	{
		modelPos.z += deltaTime * moveSpeed;
	}	

	if (glfwGetKey(window, GLFW_KEY_PAGE_DOWN ) == GLFW_PRESS)
	{
		modelPos.z -= deltaTime * moveSpeed;
	}	

	// Rotation
	if (glfwGetKey(window, GLFW_KEY_W ) == GLFW_PRESS)
	{			
		modelRot.x += deltaTime * rotSpeed;
	}	

	if (glfwGetKey(window, GLFW_KEY_S ) == GLFW_PRESS)
	{
		modelRot.x -= deltaTime * rotSpeed;
	}	

	if (glfwGetKey(window, GLFW_KEY_A ) == GLFW_PRESS)
	{
		modelRot.y -= deltaTime * rotSpeed;
	}
	
	if (glfwGetKey(window, GLFW_KEY_D ) == GLFW_PRESS)
	{
		modelRot.y += deltaTime * rotSpeed;
	}

	if (glfwGetKey(window, GLFW_KEY_E ) == GLFW_PRESS)
	{
		modelRot.z += deltaTime * rotSpeed;
	}

	if (glfwGetKey(window, GLFW_KEY_R ) == GLFW_PRESS)
	{
		modelRot.z -= deltaTime * rotSpeed;
	}

	// Model switch key
	if (spaceRel && glfwGetKey(window, GLFW_KEY_SPACE ) == GLFW_PRESS)
	{
		spaceRel = false;

		if(useModel1)
		{
			useModel1 = false;
		}
		else
		{
			if(texturePackage == 3)
			{
				useModel1 = true;
				texturePackage = 0;
			}
			else
			{
				texturePackage++;
			}				
		}			
	}

	// Increase/Decrease light strength
	if (glfwGetKey(window, GLFW_KEY_KP_ADD ) == GLFW_PRESS)
	{
		sceneLight.m_lightIntensity += deltaTime * lightIntensityChangeSpeed;

		if(sceneLight.m_lightIntensity > 1000.0f)
		{
			sceneLight.m_lightIntensity = 1000.0f;
		}
	}

	// Switch light color
	if (glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_LEFT ) == GLFW_PRESS && mouseLeftButtonRel)
	{
		mouseLeftButtonRel = false;

		switch(colorLightIndex)
		{
			// White -> Red
			case 0:
				sceneLight.m_lightColor = glm::vec3(1.0, 0.0f, 0.0f);
				colorLightIndex++;
			break;


			// Red -> Green
			case 1:
				sceneLight.m_lightColor = glm::vec3(0.0, 1.0f, 0.0f);
				colorLightIndex++;
			break;

			// Green -> Blue
			case 2:
				sceneLight.m_lightColor = glm::vec3(0.0, 0.0f, 1.0f);
				colorLightIndex++;
			break;

			// Blue -> White
			case 3:
				sceneLight.m_lightColor = glm::vec3(1.0, 1.0f, 1.0f);
				colorLightIndex = 0;
			break;
		}
	}

	if (glfwGetKey(window, GLFW_KEY_KP_SUBTRACT ) == GLFW_PRESS)
	{
		sceneLight.m_lightIntensity -= deltaTime * lightIntensityChangeSpeed;

		if(sceneLight.m_lightIntensity < 0.0f)
		{
			sceneLight.m_lightIntensity = 0.0f;
		}
	}


	if (glfwGetKey(window, GLFW_KEY_SPACE ) != GLFW_PRESS)
	{
		spaceRel = true;
	}

	if (glfwGetKey(window, GLFW_KEY_LEFT_SHIFT ) == GLFW_PRESS || glfwGetKey(window, GLFW_KEY_RIGHT_SHIFT ) == GLFW_PRESS)
	{
		shiftDown = true;
	}

	if (glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_LEFT ) != GLFW_PRESS)
	{
		mouseLeftButtonRel = true;
	}

	// Handle mouse input
	double xpos, ypos;
	glfwGetCursorPos(window, &xpos, &ypos);	
	glfwSetCursorPos(window, s_windowWidth / 2, s_windowHeight / 2);

	float deltaMouseX = (xpos - (static_cast<double>(s_windowWidth) / 2.0));
	float deltaMouseY = ((static_cast<double>(s_windowHeight) / 2.0) - ypos);

	if(fabs(deltaMouseX) > 300.0f || fabs(deltaMouseY) > 300.0f )
	{
		deltaMouseX = 0.0f;
		deltaMouseY = 0.0f;
	}

	sceneLight.m_lightPos.x += deltaMouseX * deltaTime * mouseSpeed;

	if(shiftDown)
	{
		sceneLight.m_lightPos.z -= deltaMouseY * deltaTime * mouseSpeed;
	}
	else
	{
		sceneLight.m_lightPos.y += deltaMouseY * deltaTime * mouseSpeed;
	}
}

void updateOmniShadowsLightInfo(OmniShadows &omniShadows, const SceneLightData& lightData, float nearPlane, float farPlane);

void initOmniShadows(OmniShadows &omniShadows, const SceneLightData& lightData, float nearPlane, float farPlane)
{
	omniShadows.m_shadowMapWidth = 1024;
	omniShadows.m_shadowMapHeight = 1024;

	glGenFramebuffers(1, &omniShadows.m_depthMapFBO);

	glActiveTexture(GL_TEXTURE0);
    glGenTextures(1, &omniShadows.m_depthCubemap);
    glBindTexture(GL_TEXTURE_CUBE_MAP, omniShadows.m_depthCubemap);
    for (int i = 0; i < 6; i++)
	{
        glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, 0, GL_DEPTH_COMPONENT, omniShadows.m_shadowMapWidth, omniShadows.m_shadowMapHeight, 
						0, GL_DEPTH_COMPONENT, GL_FLOAT, NULL);
	}
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);	
	
    glBindFramebuffer(GL_FRAMEBUFFER, omniShadows.m_depthMapFBO);
    glFramebufferTexture(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, omniShadows.m_depthCubemap, 0);
    glDrawBuffer(GL_NONE);
    glReadBuffer(GL_NONE);
    glBindFramebuffer(GL_FRAMEBUFFER, 0);

	omniShadows.m_pointLightMVP.resize(6);

	updateOmniShadowsLightInfo(omniShadows, lightData, nearPlane, farPlane);
}

void updateOmniShadowsLightInfo(OmniShadows &omniShadows, const SceneLightData& lightData, float nearPlane, float farPlane)
{
	float aspect = omniShadows.m_shadowMapWidth / omniShadows.m_shadowMapHeight;
	glm::mat4 shadowProj = glm::perspective(glm::radians(90.0f), aspect, nearPlane, farPlane);
	
	omniShadows.m_pointLightMVP[0] = shadowProj * glm::lookAt(lightData.m_lightPos, lightData.m_lightPos + glm::vec3( 1.0, 0.0, 0.0), glm::vec3(0.0,-1.0, 0.0));
	omniShadows.m_pointLightMVP[1] = shadowProj * glm::lookAt(lightData.m_lightPos, lightData.m_lightPos + glm::vec3(-1.0, 0.0, 0.0), glm::vec3(0.0,-1.0, 0.0));
	omniShadows.m_pointLightMVP[2] = shadowProj * glm::lookAt(lightData.m_lightPos, lightData.m_lightPos + glm::vec3( 0.0, 1.0, 0.0), glm::vec3(0.0, 0.0, 1.0));
	omniShadows.m_pointLightMVP[3] = shadowProj * glm::lookAt(lightData.m_lightPos, lightData.m_lightPos + glm::vec3( 0.0,-1.0, 0.0), glm::vec3(0.0, 0.0,-1.0));
	omniShadows.m_pointLightMVP[4] = shadowProj * glm::lookAt(lightData.m_lightPos, lightData.m_lightPos + glm::vec3( 0.0, 0.0, 1.0), glm::vec3(0.0,-1.0, 0.0));
	omniShadows.m_pointLightMVP[5] = shadowProj * glm::lookAt(lightData.m_lightPos, lightData.m_lightPos + glm::vec3( 0.0, 0.0,-1.0), glm::vec3(0.0,-1.0, 0.0));
}

void DebugReadTexture(GLuint cubeMapTexture, void **data)
{
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_CUBE_MAP, cubeMapTexture);
	glGetTexImage(GL_TEXTURE_CUBE_MAP_POSITIVE_X, 0,  GL_DEPTH_COMPONENT,  GL_FLOAT, *data);
}


void DebugUpdateCubemapTexture(GLuint cubeMapTexture, unsigned int width, unsigned int height)
{
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_CUBE_MAP, cubeMapTexture);

	float *dataRand = new float[width * height];
	for(int i = 0; i < 6; i++)
	{
		// Fill with some data
		for(int j = 0; j < width * height; j++)
		{
			dataRand[j] = static_cast<float>(rand() % 100) * 0.01f;
			//dataRand[j] = 0.5f;
		}

		//glGetTexImage(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, 0,  GL_DEPTH_COMPONENT,  GL_FLOAT, *data);
		glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, 0, GL_DEPTH_COMPONENT, width, height, 0,  GL_DEPTH_COMPONENT,  GL_FLOAT, dataRand);
	}

	delete []dataRand;
}


int main( void )
{
	GLFWwindow* window;

	DisplayStartupText();
	getchar(); // Wait for user to press Enter

	if(InitGL(&window, s_windowWidth, s_windowHeight) == -1)
	{
		return -1;
	}

	GLenum errEnum = glGetError();
	errEnum = glGetError();

 	// Hide the mouse and enable unlimited mouvement
    glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);	

	GLuint vertexArrayID;
	glGenVertexArrays(1, &vertexArrayID);
	glBindVertexArray(vertexArrayID);


	Model3dCookTorrance::InitModel3dCookTorrance();
	Text2D::InitText2D();
	TexturedQuad::InitTexturedQuad();

	std::vector<Text2D*> textControls; 
	TextControls(textControls);

	double lastTime = glfwGetTime();

	// LERP animation	
	Model3dCookTorrance* walkAnim[MAX_NR_ANIM_FRAMES];
	Model3dCookTorrance* m_lerpMdl = new Model3dCookTorrance();
	m_lerpMdl->Build("zombieWalk/zombie_walk01.obj");
	m_lerpMdl->SetTexture(GetTextureResource(ZOMBIE_ALBEDO_TEXTURE), GetTextureResource(ZOMBIE_NORMAL_TEXTURE),
										GetTextureResource(ZOMBIE_METALLIC_TEXTURE), GetTextureResource(ZOMBIE_ROUGHNESS_TEXTURE));
	for(int i = 0; i < MAX_NR_ANIM_FRAMES; i++)
	{
		if(i < 31)
		{
			Model3dCookTorrance* mdl = new Model3dCookTorrance();
			std::string mdlName = "zombieWalk/zombie_walk";

			ostringstream os;
			if(i < 9)
			{
				os << "0";
			}
			os << (i + 1) << ".obj";
			mdlName += os.str();		

			mdl->Build(mdlName.c_str());
			mdl->SetTexture(GetTextureResource(ZOMBIE_ALBEDO_TEXTURE), GetTextureResource(ZOMBIE_NORMAL_TEXTURE),
											GetTextureResource(ZOMBIE_METALLIC_TEXTURE), GetTextureResource(ZOMBIE_ROUGHNESS_TEXTURE));
			walkAnim[i] = mdl;
		}
		else
		{
			walkAnim[i] = nullptr;
		}		
	}

	Model3DCookTorranceAnimPlayer m_zombie_walk;
	m_zombie_walk.SetNrFrames(31);
	m_zombie_walk.SetFrameRate(0.02f);
	m_zombie_walk.SetFrameData(walkAnim);
	m_zombie_walk.SetPlayLoop(true);
	m_zombie_walk.SetLerpMdl(m_lerpMdl);
	
	Model3dCookTorrance* m_modelCookTorrance2 = new Model3dCookTorrance();
	m_modelCookTorrance2->Build("rubberDuck.obj");
	m_modelCookTorrance2->SetTexture(GetTextureResource(DUCK_PLASTIC_ALBEDO_TEXTURE), GetTextureResource(DUCK_PLASTIC_NORMAL_TEXTURE),
									GetTextureResource(DUCK_PLASTIC_METALLIC_TEXTURE), GetTextureResource(DUCK_PLASTIC_ROUGHNESS_TEXTURE));

	Model3dCookTorrance* m_cube_bkg = new Model3dCookTorrance();
	m_cube_bkg->Build("cube.obj");
	m_cube_bkg->SetTexture(GetTextureResource(POOL_WALLS_TEXTURE), GetTextureResource(DUCK_PLASTIC_NORMAL_TEXTURE),
									GetTextureResource(DUCK_PLASTIC_METALLIC_TEXTURE), GetTextureResource(DUCK_PLASTIC_ROUGHNESS_TEXTURE));

	Model3dCookTorrance* m_lightSource = new Model3dCookTorrance();
	m_lightSource->Build("ball.obj");
	m_lightSource->SetTexture(GetTextureResource(PROJECTILE_BALL_ALBEDO_WHITE_TEXTURE), GetTextureResource(PROJECTILE_BALL_NORMAL_TEXTURE),
									GetTextureResource(PROJECTILE_BALL_METALLIC_TEXTURE), GetTextureResource(PROJECTILE_BALL_ROUGHNESS_TEXTURE));
	const glm::vec3 lightSourceRot(0.0f, 0.0f, 0.0f);
	const glm::vec3 lightSourceScale(0.1f, 0.1f, 0.1f);
	int colorLightIndex  = 0; // Just a simple way to switch between 4 different colors - white, red, gren and blue

	glm::vec3 modelPos = glm::vec3(0.0f, -1.0f, -10.0f);
	glm::vec3 modelRot = glm::vec3(30.0f, 30.0f,  0.0f);
	glm::vec3 modelScale = glm::vec3(1.0f, 1.0f,  1.0f);

	glm::vec3 modelScaleDuck = glm::vec3(15.0f, 15.0f,  15.0f);

	glm::vec3 cubeBkgPos = glm::vec3(0.0f, -1.0f, -20.0f);
	glm::vec3 cubeBkgRot = glm::vec3(0.0f, 0.0f,  0.0f);
	glm::vec3 cubeBkgScale = glm::vec3(10.0f, 10.0f,  1.0f);

	// Basal stuff needed for rendering
	const float fov = 45.0f;
	float nearZ = 0.1f;
	float farZ = 100.0f;
	glm::mat4 projection = glm::perspective(glm::radians(fov), (float)4.0f / (float)3.0f, nearZ, farZ);	
	glm::mat4 textMVP = glm::ortho(0.0f, static_cast<float>(s_windowWidth), static_cast<float>(s_windowHeight), 0.0f);
	glm::mat4 view; // Just identity matrix
	SceneLightData sceneLight;
	InitLightData(sceneLight);

	sceneLight.m_lightPos.x = 0.0f;
	sceneLight.m_lightPos.y = 0.0f;
	sceneLight.m_lightPos.z = -5.0f;

	glm::vec4 clipPlane = glm::vec4(0.0f, 0.0f, 1.0f, farZ);

	// Omin-shadow light stuff
	OmniShadows omniShadows;
	initOmniShadows(omniShadows, sceneLight, nearZ, farZ);

	RenderStateModel3D model3DRenderState = {sceneLight, clipPlane, false, omniShadows, false};


	// For debugging depth map
	unsigned char *debugTextureData = new unsigned char[omniShadows.m_shadowMapWidth * omniShadows.m_shadowMapHeight * 3];
	float *debugDepthMapData = new float[omniShadows.m_shadowMapWidth * omniShadows.m_shadowMapHeight];
	for(int i = 0; i < omniShadows.m_shadowMapWidth * omniShadows.m_shadowMapHeight * 3; i++)
	{
		debugTextureData[i] = (char)128;
	}
	for(int i = 0; i < omniShadows.m_shadowMapWidth * omniShadows.m_shadowMapHeight; i++)
	{
		debugDepthMapData[i] = -99999.99f;
	}


	GLuint debugDepthMap = CreateTexture(debugTextureData, 1024, 1024);	
	TexturedQuad depthMapDebug(glm::vec3(512.0f, 512.0f, 0.0f), 512.0f, 512.0f, debugDepthMap);


	bool useModel1 = true;
	int texturePackage = 0;
	bool spaceRel = true;

	bool shiftDown = false;
	bool mouseLeftButtonRel = true;

	m_zombie_walk.StartPlaying();

	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
	while(glfwGetKey(window, GLFW_KEY_ESCAPE ) != GLFW_PRESS && glfwWindowShouldClose(window) == 0)
	{		
		// Time difference between frames
		double currentTime = glfwGetTime();
		float deltaTime = float(currentTime - lastTime);
		lastTime = currentTime;

		// Handle key input
		shiftDown = false;
		glfwPollEvents();

		HandleInput(window, useModel1, texturePackage, spaceRel, shiftDown, mouseLeftButtonRel, modelPos, modelRot, sceneLight, colorLightIndex, deltaTime);

		m_zombie_walk.Tick(deltaTime);
	
		// Debug depth map
		for(int i = 0; i < model3DRenderState.m_omniShadows.m_shadowMapWidth * model3DRenderState.m_omniShadows.m_shadowMapHeight; i++)
		{
			int textureIndex = i * 3;
			float depthValue = debugDepthMapData[i];

			//debugTextureData[textureIndex + 0] = debugDepthMapData[i]
			int kalle = 5;
			kalle++;
		}

		// Shadow pass
		updateOmniShadowsLightInfo(model3DRenderState.m_omniShadows, model3DRenderState.m_sceneLightData, nearZ, farZ);
		//////////////////////////////////////////////////////////////////////////////////		
		model3DRenderState.m_renderShadowDataPass = true;
		Model3dCookTorrance::PrepareRender(model3DRenderState);		
		if(useModel1)
		{
			m_zombie_walk.SetPos(modelPos);
			m_zombie_walk.SetRot(modelRot);
			m_zombie_walk.SetScale(modelScale);
			m_zombie_walk.Render(model3DRenderState, false, projection, view);
		}
		else
		{
			m_modelCookTorrance2->Render(model3DRenderState, projection, view, modelPos, modelRot, modelScaleDuck);
		}
		m_cube_bkg->Render(model3DRenderState, projection, view, cubeBkgPos, cubeBkgRot, cubeBkgScale);
		Model3dCookTorrance::AfterRender(model3DRenderState);
		model3DRenderState.m_renderShadowDataPass = false;
		//////////////////////////////////////////////////////////////////////////////

		//***** DEBUG *****
		// Fill depth cubemap with rand values to see if we are reading it back in the shader
		//DebugUpdateCubemapTexture(model3DRenderState.m_omniShadows.m_depthCubemap, 1024, 1024);
		//***** DEBUG *****
		
		// Debug depth map
		errEnum = glGetError();
		DebugReadTexture(model3DRenderState.m_omniShadows.m_depthCubemap, (void**)&debugDepthMapData);
		errEnum = glGetError();
		for(int i = 0; i < model3DRenderState.m_omniShadows.m_shadowMapWidth * model3DRenderState.m_omniShadows.m_shadowMapHeight; i++)
		{
			int textureIndex = i * 3;
			float depthValue = debugDepthMapData[i];

			float RGBColValue = 255.0f * depthValue; // debugDepthMapData values are in range 0, 1 - bring them to 0, 255
			unsigned int RGBColAsInt = (unsigned int)RGBColValue;			
			debugTextureData[textureIndex + 0] = RGBColAsInt;
			debugTextureData[textureIndex + 1] = RGBColAsInt;
			debugTextureData[textureIndex + 2] = RGBColAsInt;
			//int kalle = 5;
			//kalle++;
		}
		//void UpdateTexture(GLuint texture, const unsigned char* data, const int width, const int height)
		UpdateTexture(debugDepthMap, debugTextureData, 1024, 1024);

		//////////////////////////////////////////////////////////////////////////////		
		// Cook-torrance light render pass		
		Model3dCookTorrance::PrepareRender(model3DRenderState);
		glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		if(useModel1)
		{
			m_zombie_walk.SetPos(modelPos);
			m_zombie_walk.SetRot(modelRot);
			m_zombie_walk.SetScale(modelScale);
			m_zombie_walk.Render(model3DRenderState, false, projection, view);
		}
		else
		{
			switch(texturePackage)
			{
				case 0:
					m_modelCookTorrance2->SetTexture(GetTextureResource(DUCK_PLASTIC_ALBEDO_TEXTURE), GetTextureResource(DUCK_PLASTIC_NORMAL_TEXTURE),
													GetTextureResource(DUCK_PLASTIC_METALLIC_TEXTURE), GetTextureResource(DUCK_PLASTIC_ROUGHNESS_TEXTURE));
				break;

				case 1:
					m_modelCookTorrance2->SetTexture(GetTextureResource(DUCK_WOOD_ALBEDO_TEXTURE), GetTextureResource(DUCK_PLASTIC_NORMAL_TEXTURE),
													GetTextureResource(DUCK_WOOD_METALLIC_TEXTURE), GetTextureResource(DUCK_WOOD_ROUGHNESS_TEXTURE));
				break;

				case 2:
					m_modelCookTorrance2->SetTexture(GetTextureResource(DUCK_METAL_ALBEDO_TEXTURE), GetTextureResource(DUCK_PLASTIC_NORMAL_TEXTURE),
													GetTextureResource(DUCK_METAL_METALLIC_TEXTURE), GetTextureResource(DUCK_METAL_ROUGHNESS_TEXTURE));
				break;

				case 3:
					m_modelCookTorrance2->SetTexture(GetTextureResource(DUCK_GOLD_ALBEDO_TEXTURE), GetTextureResource(DUCK_PLASTIC_NORMAL_TEXTURE),
													GetTextureResource(DUCK_GOLD_METALLIC_TEXTURE), GetTextureResource(DUCK_GOLD_ROUGHNESS_TEXTURE));
				break;

			}
			m_modelCookTorrance2->Render(model3DRenderState, projection, view, modelPos, modelRot, modelScaleDuck);
		}		

		m_cube_bkg->Render(model3DRenderState, projection, view, cubeBkgPos, cubeBkgRot, cubeBkgScale);
		m_lightSource->Render(model3DRenderState, projection, view, sceneLight.m_lightPos, lightSourceRot, lightSourceScale);
		
		Model3dCookTorrance::AfterRender(model3DRenderState);
		//////////////////////////////////////////////////////////////////////////////		

		//depthMapDebug.Render(textMVP, view);

		for(auto text : textControls)
		{
			text->Render(textMVP, view);
		}		

		glfwSwapBuffers(window);
	}

	ClearTextureCache();

	delete m_modelCookTorrance2;
	delete m_cube_bkg;
	delete m_lightSource;

	delete m_lerpMdl;
	for(int i = 0; i < 31; i++)
	{
		delete walkAnim[i];
	}

	delete []debugTextureData;

	for(auto text : textControls)
	{
		delete text;
	}
	// Clean up and close OpenGL
	glDeleteVertexArrays(1, &vertexArrayID);	
	glfwTerminate();

	return 0;
}

