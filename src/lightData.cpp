#include <iostream>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include "lightData.h"

void InitLightData(SceneLightData &lightData)
{
	lightData.m_lightPos = glm::vec3(0.0f, 0.0f, 2.0f);
	lightData.m_lightColor = glm::vec3(1.0f , 1.0f , 1.0f);
	lightData.m_lightIntensity = 70.0f;
}

