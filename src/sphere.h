#ifndef SPHERE_H
#define SPHERE_H

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

class Sphere
{
	public:
	Sphere();
	Sphere(glm::vec3 pos, float radius);
	~Sphere();

	const glm::vec3 & GetPos() const;
	const float & GetRadius() const;

	private:

	glm::vec3	m_pos;
	float		m_radius;
};

#endif