#ifndef HANDLEINPUT_H
#define HANDLEINPUT_H

#include <glm/glm.hpp>
#include <memory>

// Foward declarations
class Player;

class InputState
{
    public:
    InputState();
    ~InputState();

    double  PressedFireBallLastTime() const;
    void    PressedFireBallLastTime(double lastTime);

    bool    PressedFireBallFirstTime() const;
    void    PressedFireBallFirstTime(bool pressedFireBallFirstTime);


    double  PlayerTwoPressedFireBallLastTime() const;
    void    PlayerTwoPressedFireBallLastTime(double lastTime);

    bool    PlayerTwoPressedFireBallFirstTime() const;
    void    PlayerTwoPressedFireBallFirstTime(bool pressedFireBallFirstTime);

    bool    EnterKeyReleased() const;
    void    EnterKeyReleased(bool enterKeyReleased);

    private:
    double  m_pressedFireBallLastTime;
    bool    m_pressedFireBallFirstTime;            

    double  m_playerTwoPressedFireBallLastTime;
    bool    m_playerTwoPressedFireBallFirstTime;

    bool    m_enterKeyReleased;            
    
    
};

void HandleKeyInput(GLFWwindow* window, InputState &inputState, double deltaTime, Player *playerOne, Player* playerTwo, 
                    bool &restartGame, bool gameOver);

#endif