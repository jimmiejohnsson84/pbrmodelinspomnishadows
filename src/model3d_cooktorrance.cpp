// Include standard headers
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <vector>
using namespace std;

// Include GLEW
#include <GL/glew.h>

// Include GLFW
#include <GLFW/glfw3.h>

// Include GLM
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
using namespace glm;

#include "objloader.h"
#include "model3d_cooktorrance.h"
#include "shader.h"
#include "lightData.h"
#include "renderStruct.h"
#include "initGL.h"

Model3dCookTorrance::shaderProgramHandles Model3dCookTorrance::m_model3DCookTorranceShaderHandles;

Model3dCookTorrance::shaderShadowProgramHandles Model3dCookTorrance::m_model3DShadowShaderHandles;


void Model3dCookTorrance::InitModel3dCookTorrance()
{
	m_model3DCookTorranceShaderHandles.m_programID = LoadShaders( "shaders/model3d_cooktorrance.vertexshader", "shaders/model3d_cooktorrance.fragmentshader" );	

	// Get handles for data in the shader
	m_model3DCookTorranceShaderHandles.m_modelViewProjectionID = glGetUniformLocation(m_model3DCookTorranceShaderHandles.m_programID, "MVP");
	m_model3DCookTorranceShaderHandles.m_viewMatrixID = glGetUniformLocation(m_model3DCookTorranceShaderHandles.m_programID, "V");
	m_model3DCookTorranceShaderHandles.m_modelMatrixID = glGetUniformLocation(m_model3DCookTorranceShaderHandles.m_programID, "M");
	m_model3DCookTorranceShaderHandles.m_modelViewMatrixID = glGetUniformLocation(m_model3DCookTorranceShaderHandles.m_programID, "MV");
	m_model3DCookTorranceShaderHandles.m_modelView3By3MatrixID = glGetUniformLocation(m_model3DCookTorranceShaderHandles.m_programID, "MV3By3");
	m_model3DCookTorranceShaderHandles.m_normalMatrixID = glGetUniformLocation(m_model3DCookTorranceShaderHandles.m_programID, "NM");
	m_model3DCookTorranceShaderHandles.m_lightPos_worldspaceID = glGetUniformLocation(m_model3DCookTorranceShaderHandles.m_programID, "lightPosition_worldspace");
	m_model3DCookTorranceShaderHandles.m_lightColorID = glGetUniformLocation(m_model3DCookTorranceShaderHandles.m_programID, "lightColor");
	m_model3DCookTorranceShaderHandles.m_lightIntensityID = glGetUniformLocation(m_model3DCookTorranceShaderHandles.m_programID, "lightIntensity");

	m_model3DCookTorranceShaderHandles.m_farPlaneID = glGetUniformLocation(m_model3DCookTorranceShaderHandles.m_programID, "farPlane");
	
	m_model3DCookTorranceShaderHandles.m_textureAlbedoID = glGetUniformLocation(m_model3DCookTorranceShaderHandles.m_programID, "textureAlbedo");
	m_model3DCookTorranceShaderHandles.m_textureNormalID = glGetUniformLocation(m_model3DCookTorranceShaderHandles.m_programID, "textureNormal");
	m_model3DCookTorranceShaderHandles.m_textureMetallicID = glGetUniformLocation(m_model3DCookTorranceShaderHandles.m_programID, "textureMetallic");
	m_model3DCookTorranceShaderHandles.m_textureRoughnessID = glGetUniformLocation(m_model3DCookTorranceShaderHandles.m_programID, "textureRoughness");

	m_model3DCookTorranceShaderHandles.m_depthMapID = glGetUniformLocation(m_model3DCookTorranceShaderHandles.m_programID, "depthMap");

	m_model3DCookTorranceShaderHandles.m_userClipPlane0ID = glGetUniformLocation(m_model3DCookTorranceShaderHandles.m_programID, "userClipPlane0");

	// For shadows
	m_model3DShadowShaderHandles.m_programID = LoadShaders("shaders/point_shadow.vertexshader", "shaders/point_shadow.fragmentshader", "shaders/point_shadow.geometryshader");

	m_model3DShadowShaderHandles.m_modelID = glGetUniformLocation(m_model3DShadowShaderHandles.m_programID, "model");
	m_model3DShadowShaderHandles.m_shadowMatricesID[0] = glGetUniformLocation(m_model3DShadowShaderHandles.m_programID, "shadowMatrices[0]");
	m_model3DShadowShaderHandles.m_shadowMatricesID[1] = glGetUniformLocation(m_model3DShadowShaderHandles.m_programID, "shadowMatrices[1]");
	m_model3DShadowShaderHandles.m_shadowMatricesID[2] = glGetUniformLocation(m_model3DShadowShaderHandles.m_programID, "shadowMatrices[2]");

	m_model3DShadowShaderHandles.m_shadowMatricesID[3] = glGetUniformLocation(m_model3DShadowShaderHandles.m_programID, "shadowMatrices[3]");
	m_model3DShadowShaderHandles.m_shadowMatricesID[4] = glGetUniformLocation(m_model3DShadowShaderHandles.m_programID, "shadowMatrices[4]");
	m_model3DShadowShaderHandles.m_shadowMatricesID[5] = glGetUniformLocation(m_model3DShadowShaderHandles.m_programID, "shadowMatrices[5]");
	m_model3DShadowShaderHandles.m_farPlaneID = glGetUniformLocation(m_model3DShadowShaderHandles.m_programID, "farPlane");
	m_model3DShadowShaderHandles.m_lightPos_worldspaceID = glGetUniformLocation(m_model3DShadowShaderHandles.m_programID, "lightPos_worldspace");
	
}
//----------------------------------------------------------------------
void Model3dCookTorrance::PrepareRender(const RenderStateModel3D& renderState)
{
	if(renderState.m_renderShadowDataPass)
	{
		glUseProgram(m_model3DShadowShaderHandles.m_programID);
		glViewport(0, 0, renderState.m_omniShadows.m_shadowMapWidth, renderState.m_omniShadows.m_shadowMapHeight);
		glBindFramebuffer(GL_FRAMEBUFFER, renderState.m_omniShadows.m_depthMapFBO);
		
		glClear(GL_DEPTH_BUFFER_BIT);
		glEnableVertexAttribArray(0);
	}
	else
	{
		glUseProgram(m_model3DCookTorranceShaderHandles.m_programID);
		glEnableVertexAttribArray(0);
		glEnableVertexAttribArray(1);
		glEnableVertexAttribArray(2);
		glEnableVertexAttribArray(3);
	}
}
//----------------------------------------------------------------------
void Model3dCookTorrance::AfterRender(const RenderStateModel3D& renderState)
{
	if(renderState.m_renderShadowDataPass)
	{
		glBindFramebuffer(GL_FRAMEBUFFER, 0);
		glViewport(0, 0, s_windowWidth, s_windowHeight);
		glDisableVertexAttribArray(0);
	}
	else
	{
		glDisableVertexAttribArray(0);
		glDisableVertexAttribArray(1);
		glDisableVertexAttribArray(2);
		glDisableVertexAttribArray(3);		
	}
}
//----------------------------------------------------------------------
Model3dCookTorrance::Model3dCookTorrance()
{
	m_vertexBuffer = 0;
	m_normalBuffer = 0;
	m_tangents = 0;
	m_vertexBufferSize = 0;
	m_built = false;
}
//----------------------------------------------------------------------
Model3dCookTorrance::~Model3dCookTorrance()
{
	if(m_built)
	{
		glDeleteBuffers(1, &m_vertexBuffer);
		glDeleteBuffers(1, &m_normalBuffer);
		glDeleteBuffers(1, &m_uvCoords);		
		glDeleteBuffers(1, &m_tangents);
	}
}
//----------------------------------------------------------------------
void Model3dCookTorrance::Build(const char *modelPath)
 {
	std::vector<glm::vec2> uvs;
	std::vector<glm::vec3> normals;

	std::vector<glm::vec3> tangents;
	std::vector<glm::vec3> bitangents;

	bool loadStatus = loadOBJ(modelPath, m_vertices, uvs, normals);
	if(loadStatus)
	{
		// Construct tangent & bitangent data for normal mapping
		// Assumes we are using only triangles, we work on each triangle to create the tangent & bitanget
		bool uvDirSet = false;
		float posXDir = 0.0f;


		for(int i = 0; i < m_vertices.size(); i+= 3)
		{
				glm::vec3& v0 = m_vertices[i + 0];
				glm::vec3& v1 = m_vertices[i + 1];
				glm::vec3& v2 = m_vertices[i + 2];

				glm::vec2& uv0 = uvs[i + 0];
				glm::vec2& uv1 = uvs[i + 1];
				glm::vec2& uv2 = uvs[i + 2];

				// Delta in model space				
				glm::vec3 deltaPos1 = v1 - v0;
				glm::vec3 deltaPos2 = v2 - v0;
	

				// Delta in "texture coordinate space"
				glm::vec2 deltaUV1 = uv1 - uv0;
				glm::vec2 deltaUV2 = uv2 - uv0;

				/* 	Time to calculate tangent & bitanget
					We could theoretically choose any tangent & bitanget given triangle (v1 - v0, v2 - v0), but we will
					chose them with respect to both orientation of the triangle and the uv delta

					There is a bit of matrix math involved in calculating this stuff - look into the details at a later stage.
				*/
				float f = 1.0f / (deltaUV1.x * deltaUV2.y - deltaUV1.y * deltaUV2.x);
 				glm::vec3 tangent = f * (deltaPos1 * deltaUV2.y - deltaPos2 * deltaUV1.y);
				glm::vec3 bitangent = f * (deltaPos2 * deltaUV1.x - deltaPos1 * deltaUV2.x);

				tangents.push_back(tangent);
				tangents.push_back(tangent);
				tangents.push_back(tangent);

				bitangents.push_back(bitangent);
				bitangents.push_back(bitangent);
				bitangents.push_back(bitangent);

		}

		for (unsigned int i = 0; i < m_vertices.size(); i++)
		{		
			glm::vec3 & n = normals[i];
			glm::vec3 & t = tangents[i];
			glm::vec3 & b = bitangents[i];
			
			// Gram-Schmidt orthogonalize
			t = glm::normalize(t - n * glm::dot(n, t));
			
			// Calculate handedness
			if (glm::dot(glm::cross(n, t), b) < 0.0f)
			{
				t = t * -1.0f;
			}
		}

		glGenBuffers(1, &m_vertexBuffer);
		glBindBuffer(GL_ARRAY_BUFFER, m_vertexBuffer);
		glBufferData(GL_ARRAY_BUFFER, m_vertices.size() * sizeof(glm::vec3), &m_vertices[0], GL_STATIC_DRAW);

		glGenBuffers(1, &m_normalBuffer);
		glBindBuffer(GL_ARRAY_BUFFER, m_normalBuffer);
		glBufferData(GL_ARRAY_BUFFER, normals.size() * sizeof(glm::vec3), &normals[0], GL_STATIC_DRAW);		

		glGenBuffers(1, &m_uvCoords);
		glBindBuffer(GL_ARRAY_BUFFER, m_uvCoords);
		glBufferData(GL_ARRAY_BUFFER, uvs.size() * sizeof(glm::vec2), &uvs[0], GL_STATIC_DRAW);	

		glGenBuffers(1, &m_tangents);
		glBindBuffer(GL_ARRAY_BUFFER, m_tangents);
		glBufferData(GL_ARRAY_BUFFER, tangents.size() * sizeof(glm::vec3), &tangents[0], GL_STATIC_DRAW);
	}

	m_vertexBufferSize = m_vertices.size();
	m_built = loadStatus;
 }
//----------------------------------------------------------------------
void Model3dCookTorrance::SetTexture(GLuint textureAlbedo, GLuint textureNormal, GLuint textureMetallic, GLuint textureRoughness)
{
	m_textureAlbedo = textureAlbedo;
	m_textureNormal = textureNormal;
	m_textureMetallic = textureMetallic;
	m_textureRoughness = textureRoughness;
}
//----------------------------------------------------------------------
void Model3dCookTorrance::Lerp(const std::vector<glm::vec3>& frame1, const std::vector<glm::vec3>& frame2, float t)
{
	for(int i = 0; i < frame1.size(); i++)
	{
		m_vertices[i] = glm::mix(frame1[i], frame2[i], t);		
	}

	glBindBuffer(GL_ARRAY_BUFFER, m_vertexBuffer);
	glBufferSubData(GL_ARRAY_BUFFER, 0, m_vertices.size() * sizeof(glm::vec3), &m_vertices[0]);
}
//----------------------------------------------------------------------
void Model3dCookTorrance::Render(const RenderStateModel3D& renderState, const glm::mat4 &projection, 
						const glm::mat4 &view, const glm::vec3 &pos, const glm::vec3 &rot, const glm::vec3 &scale)
{

	float rotationY = 0.0f;
	glm::mat4 model = glm::mat4(1.0f);
	glm::mat4 rotateBase = glm::rotate(model, glm::radians(rot.x), glm::vec3(1, 0, 0));
	rotateBase = rotateBase * glm::rotate(model, glm::radians(rot.y), glm::vec3(0, 1, 0));
	rotateBase = rotateBase * glm::rotate(model, glm::radians(rot.z), glm::vec3(0, 0, 1));
	glm::mat4 translateBase = glm::translate(model, pos);

	glm::mat4 scaleBase = glm::scale(model, scale);
	model =  translateBase * rotateBase * scaleBase;
	glm::mat4 modelView = view * model;
	glm::mat3 modelView3By3 = glm::mat3(modelView); // top left part of modelView
	glm::mat4 modelViewProjection = projection * modelView;
	glm::mat4 normalMatrix = glm::transpose(glm::inverse(modelView));

	if(renderState.m_renderShadowDataPass)
	{
		RenderShadowPass(renderState, model);
	}
	else
	{	
		// Forward to shader
		glUniformMatrix4fv(m_model3DCookTorranceShaderHandles.m_modelViewProjectionID, 1, GL_FALSE, &modelViewProjection[0][0]);
		glUniformMatrix4fv(m_model3DCookTorranceShaderHandles.m_modelMatrixID, 1, GL_FALSE, &model[0][0]);
		glUniformMatrix4fv(m_model3DCookTorranceShaderHandles.m_viewMatrixID, 1, GL_FALSE, &view[0][0]);	
		
		glUniformMatrix4fv(m_model3DCookTorranceShaderHandles.m_modelViewMatrixID, 1, GL_FALSE, &modelView[0][0]);
		glUniformMatrix3fv(m_model3DCookTorranceShaderHandles.m_modelView3By3MatrixID, 1, GL_FALSE, &modelView3By3[0][0]);
		glUniformMatrix4fv(m_model3DCookTorranceShaderHandles.m_normalMatrixID, 1, GL_FALSE, &normalMatrix[0][0]);

		// Set clip plane (used only when rendering water reflection surface)
		glUniform4f(m_model3DCookTorranceShaderHandles.m_userClipPlane0ID, renderState.m_clipPlane.x, renderState.m_clipPlane.y, 
					renderState.m_clipPlane.z, renderState.m_clipPlane.w);

		// Light
		glm::vec3 lightPos = renderState.m_sceneLightData.m_lightPos;
		if(renderState.m_invertLightPos)
		{
			lightPos.z = -lightPos.z;
		}

		glUniform3f(m_model3DCookTorranceShaderHandles.m_lightPos_worldspaceID, lightPos.x, lightPos.y, lightPos.z);

		glUniform3f(m_model3DCookTorranceShaderHandles.m_lightColorID, renderState.m_sceneLightData.m_lightColor.x, 
					renderState.m_sceneLightData.m_lightColor.y, renderState.m_sceneLightData.m_lightColor.z);

		glUniform1f(m_model3DCookTorranceShaderHandles.m_lightIntensityID, renderState.m_sceneLightData.m_lightIntensity);

		const float farPlane = 100.0f; // For now lets let this be hard coded here
		glUniform1f(m_model3DCookTorranceShaderHandles.m_farPlaneID, farPlane);
		
		// Bind textures
		glActiveTexture(GL_TEXTURE0);		
		glBindTexture(GL_TEXTURE_2D, m_textureAlbedo);

		glActiveTexture(GL_TEXTURE1);
		glBindTexture(GL_TEXTURE_2D, m_textureNormal);

		glActiveTexture(GL_TEXTURE2);
		glBindTexture(GL_TEXTURE_2D, m_textureMetallic);

		glActiveTexture(GL_TEXTURE3);
		glBindTexture(GL_TEXTURE_2D, m_textureRoughness);
		
		glActiveTexture(GL_TEXTURE4);
		glBindTexture(GL_TEXTURE_CUBE_MAP, renderState.m_omniShadows.m_depthCubemap);
		
		// Align up with shader
		glUniform1i(m_model3DCookTorranceShaderHandles.m_textureAlbedoID, 0);
		glUniform1i(m_model3DCookTorranceShaderHandles.m_textureNormalID, 1);
		glUniform1i(m_model3DCookTorranceShaderHandles.m_textureMetallicID, 2);
		glUniform1i(m_model3DCookTorranceShaderHandles.m_textureRoughnessID, 3);
		glUniform1i(m_model3DCookTorranceShaderHandles.m_depthMapID, 4);

		// Enable vertex & normal attribute arrays		
		glBindBuffer(GL_ARRAY_BUFFER, m_vertexBuffer);
		glVertexAttribPointer(
			0,
			3,
			GL_FLOAT,
			GL_FALSE,
			0,
			(void*)0
		);

		glBindBuffer(GL_ARRAY_BUFFER, m_normalBuffer);
		glVertexAttribPointer(
			1,
			3,
			GL_FLOAT,
			GL_FALSE,
			0,
			(void*)0
		);

		glBindBuffer(GL_ARRAY_BUFFER, m_uvCoords);
		glVertexAttribPointer(
			2,
			2,
			GL_FLOAT,
			GL_FALSE,
			0,
			(void*)0
		);		

		glBindBuffer(GL_ARRAY_BUFFER, m_tangents);
		glVertexAttribPointer(
			3,
			3,
			GL_FLOAT,
			GL_FALSE,
			0,
			(void*)0
		);

		glDrawArrays(GL_TRIANGLES, 0, m_vertexBufferSize);
	}
}
//----------------------------------------------------------------------
void Model3dCookTorrance::RenderShadowPass(const RenderStateModel3D& renderState, const glm::mat4 &model)
{
	// Forward to shader
	glUniformMatrix4fv(m_model3DShadowShaderHandles.m_modelID, 1, GL_FALSE, &model[0][0]);
	glUniformMatrix4fv(m_model3DShadowShaderHandles.m_shadowMatricesID[0], 1, GL_FALSE, &renderState.m_omniShadows.m_pointLightMVP[0][0][0]);
	glUniformMatrix4fv(m_model3DShadowShaderHandles.m_shadowMatricesID[1], 1, GL_FALSE, &renderState.m_omniShadows.m_pointLightMVP[1][0][0]);
	glUniformMatrix4fv(m_model3DShadowShaderHandles.m_shadowMatricesID[2], 1, GL_FALSE, &renderState.m_omniShadows.m_pointLightMVP[2][0][0]);

	glUniformMatrix4fv(m_model3DShadowShaderHandles.m_shadowMatricesID[3], 1, GL_FALSE, &renderState.m_omniShadows.m_pointLightMVP[3][0][0]);
	glUniformMatrix4fv(m_model3DShadowShaderHandles.m_shadowMatricesID[4], 1, GL_FALSE, &renderState.m_omniShadows.m_pointLightMVP[4][0][0]);
	glUniformMatrix4fv(m_model3DShadowShaderHandles.m_shadowMatricesID[5], 1, GL_FALSE, &renderState.m_omniShadows.m_pointLightMVP[5][0][0]);

	glUniform3f(m_model3DShadowShaderHandles.m_lightPos_worldspaceID, renderState.m_sceneLightData.m_lightPos.x,
				renderState.m_sceneLightData.m_lightPos.y, renderState.m_sceneLightData.m_lightPos.z);

	const float farPlane = 100.0f; // For now lets let this be hard coded here
	glUniform1f(m_model3DShadowShaderHandles.m_farPlaneID, farPlane);

	// Send vertices to the shadow pass
	glBindBuffer(GL_ARRAY_BUFFER, m_vertexBuffer);
	glVertexAttribPointer(
		0,
		3,
		GL_FLOAT,
		GL_FALSE,
		0,
		(void*)0
	);

	glDrawArrays(GL_TRIANGLES, 0, m_vertexBufferSize);
}


