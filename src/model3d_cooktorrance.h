#ifndef MODEL3D_COOK_TORRANCE_H
#define MODEL3D_COOK_TORRANCE_H


// Fwds
struct RenderStateModel3D;

class Model3dCookTorrance
{
	public:
	Model3dCookTorrance();
	~Model3dCookTorrance();

	void	Build(const char *modelPath);
	void	SetTexture(GLuint textureAlbedo, GLuint textureNormal, GLuint textureMetallic, GLuint textureRoughness);

	const std::vector<glm::vec3>& GetVertices() const { return m_vertices;}

	void	Lerp(const std::vector<glm::vec3>& frame1, const std::vector<glm::vec3>& frame2, float t);

	void	Render(const RenderStateModel3D& renderState, const glm::mat4 &projection, 
					const glm::mat4 &view, const glm::vec3 &pos, const glm::vec3 &rot, const glm::vec3 &scale);

	static void InitModel3dCookTorrance();
	static void PrepareRender(const RenderStateModel3D& renderState);
	static void AfterRender(const RenderStateModel3D& renderState);

	private:

	void	RenderShadowPass(const RenderStateModel3D& renderState, const glm::mat4 &model);

	// To support LERP
	std::vector<glm::vec3>	m_vertices;									

	// BOs containing vertex and normal data for the model
	GLuint	m_normalBuffer;
	GLuint	m_vertexBuffer;
	GLuint	m_uvCoords;
	GLuint	m_tangents;

	// Material description (textures)
	GLuint m_textureAlbedo;
	GLuint m_textureNormal;
	GLuint m_textureMetallic;
	GLuint m_textureRoughness;

	unsigned int	m_vertexBufferSize;
	unsigned int	m_indicesSize;

	// Shader handles
	struct shaderProgramHandles
	{
		GLuint m_programID;
		GLuint m_lightPos_worldspaceID;
		GLuint m_viewMatrixID;
		GLuint m_modelViewProjectionID;
		GLuint m_modelMatrixID;
		GLuint m_modelViewMatrixID;
		GLuint m_modelView3By3MatrixID;
		GLuint m_normalMatrixID;
		GLuint m_userClipPlane0ID;

		GLuint m_lightColorID;
		GLuint m_lightIntensityID;

		GLuint m_farPlaneID; // Used for shadow calculations

		// Textures
		GLuint m_textureAlbedoID;
		GLuint m_textureNormalID;
		GLuint m_textureMetallicID;
		GLuint m_textureRoughnessID;

		// Cubemap (depth map for shadows)
		GLuint m_depthMapID;
	};

	struct shaderShadowProgramHandles
	{
		GLuint m_programID;
		GLuint m_modelID;
		GLuint m_shadowMatricesID[6];
		GLuint m_lightPos_worldspaceID;
		GLuint m_farPlaneID;
	};

	static shaderProgramHandles m_model3DCookTorranceShaderHandles;
	static shaderShadowProgramHandles m_model3DShadowShaderHandles;
	
	// If true, BOs have been built
	bool			m_built;
};

#endif