#ifndef ANIM_PLAYER_H
#define ANIM_PLAYER_H

#include "geometryData.h"


// Fwds
class Model3dCookTorrance;
struct RenderStateModel3D;

#define MAX_NR_ANIM_FRAMES 50

class Model3DCookTorranceAnimPlayer
{
    public:
    Model3DCookTorranceAnimPlayer();
    ~Model3DCookTorranceAnimPlayer();

    void        SetPos(glm::vec3 pos) { m_geometryData.m_pos = pos;}
    void        SetRot(glm::vec3 rot) { m_geometryData.m_rot = rot; }
    void        SetScale(glm::vec3 scale) { m_geometryData.m_scale = scale; }

    void        SetFrameRate(float v) { m_frameRate = v;}
    void        SetNrFrames(int nrFrames) {m_animData.m_nrFrames = nrFrames;}
    void        SetFrameData(Model3dCookTorrance* frames[MAX_NR_ANIM_FRAMES]);
    void        SetLerpMdl(Model3dCookTorrance* lerpMdl) { m_lerpMdl = lerpMdl;}
    void        SetPlayLoop(bool v) { m_loop = v;}

    bool        GetIsPlaying() const { return m_animPlaying; }
    bool        GetIsOnDoDamageFrame() const    {
                                                 return (m_animData.m_currentFrame == m_animData.m_nrFrames - 1 ||
                                                        m_animData.m_currentFrame == m_animData.m_nrFrames - 2);
                                                }

    void        StartPlaying();

    void        Tick(float deltaTime);

    void        Render(const RenderStateModel3D& renderState, bool renderingReflection, 
                        const glm::mat4 &projection, const glm::mat4 &view);	

    private:

    void        ChangeFrameDelta(int v);
    void        ChangeFrameNr(int frame);
    
    struct AnimData
    {
        Model3dCookTorrance*    m_frames[MAX_NR_ANIM_FRAMES];
        int                     m_nrFrames;
        int                     m_currentFrame;
    };

    Model3dCookTorrance*    m_lerpMdl;
    AnimData                m_animData;
    GeometryData            m_geometryData;
    
    
    float       m_timeLast;
    float       m_frameRate;
    float       m_lerpFactor;
    
    bool        m_animPlaying;
    bool        m_playReverse;
    bool        m_playFwdThenReverse;
    bool        m_loop;
};


#endif