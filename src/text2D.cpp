#include <vector>
#include <iostream>
#include <cstring>

#include <GL/glew.h>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
using namespace glm;

#include "shader.h"
#include "texture.h"

#include "text2D.h"

using namespace std;


// Statics
GLuint Text2D::m_programID;
GLuint Text2D::m_mvpID;
GLuint Text2D::m_textureID;
GLuint Text2D::m_text2DTextureID;

// 26 chars + 10 digits + 4 special chars (space, comma, '-' and '+')
#define NR_CHARS_SUPPORTED (26 + 10 + 4)


Text2D::Text2D(char *text, glm::vec3 start, float size)
{
	m_text = new char[strlen(text) + 1];
	memcpy(m_text, text, strlen(text) + 1);
	m_start = start;
	m_size = size;

	m_textLength = strlen(text);

	CreateTextQuads();
	BuildVBOS();	
}
//----------------------------------------------------------------------
Text2D::~Text2D()
{
	glDeleteBuffers(1, &m_verticesVBO);
	glDeleteBuffers(1, &m_verticesUVVBO);
	
	delete[] m_text;
}
//----------------------------------------------------------------------
void Text2D::InitText2D()
{
	// Initialize texture
	m_text2DTextureID = GetTextureResource(HUD_TEXT2D_FONT, false);

	m_programID = LoadShaders( "shaders/text.vertexshader", "shaders/text.fragmentshader" );		
	m_textureID = glGetUniformLocation(m_programID, "myTextureSampler");
	m_mvpID = glGetUniformLocation(m_programID, "MVP");
}
//----------------------------------------------------------------------
void Text2D::BuildVBOS()
{
	glGenBuffers(1, &m_verticesVBO);
	glGenBuffers(1, &m_verticesUVVBO);

	glBindBuffer(GL_ARRAY_BUFFER, m_verticesVBO);
	glBufferData(GL_ARRAY_BUFFER, m_vertices.size() * sizeof(glm::vec3), &m_vertices[0], GL_STATIC_DRAW);

	glBindBuffer(GL_ARRAY_BUFFER, m_verticesUVVBO);
	glBufferData(GL_ARRAY_BUFFER, m_verticesUV.size() * sizeof(glm::vec2), &m_verticesUV[0], GL_STATIC_DRAW);		
}
//----------------------------------------------------------------------
void Text2D::CreateTextQuads()
{
	for ( unsigned int i = 0 ; i < m_textLength ; i++ )
	{		

	/*
		----> X
		|
		|
		|
		Y

		P1     P2

		P4     P3
	*/	
		glm::vec3 P1 = glm::vec3( (static_cast<float>(i) * m_size) + m_start.x - m_size / 2.0f, m_start.y - m_size / 2.0f, m_start.z);
		glm::vec3 P2 = glm::vec3( (static_cast<float>(i) * m_size) + m_start.x + m_size / 2.0f, m_start.y - m_size / 2.0f, m_start.z);
		glm::vec3 P3 = glm::vec3( (static_cast<float>(i) * m_size) + m_start.x + m_size / 2.0f, m_start.y + m_size / 2.0f, m_start.z);
		glm::vec3 P4 = glm::vec3( (static_cast<float>(i) * m_size) + m_start.x - m_size / 2.0f, m_start.y + m_size / 2.0f, m_start.z);

		
		m_vertices.push_back(P3);
		m_vertices.push_back(P2);
		m_vertices.push_back(P1);

		m_vertices.push_back(P1);
		m_vertices.push_back(P4);
		m_vertices.push_back(P3);


		// 'A' in ASCII is 65, '0' in ASCII is 48.		
		char character = m_text[i];
		int theChar = static_cast<int>(character);

		float digUScale = 1.0f /  static_cast<float>(NR_CHARS_SUPPORTED);
		float uv_x = 0.0f;
		if(theChar >= 65)
		{
			uv_x = static_cast<float>(character - 65) * digUScale;
		}
		else if(theChar >= 48 && theChar <= 57)
		{
			uv_x = static_cast<float>(26)* digUScale + static_cast<float>(character - 48) * digUScale;
		}
		else if(theChar == 32) // Spacebar
		{
			uv_x = static_cast<float>(36) * digUScale;
		}
		else if(theChar == 44 || theChar == 46)// Comma or dot
		{
			uv_x = static_cast<float>(37) * digUScale;
		}
		else if(theChar == 45) // '-'
		{
			uv_x = static_cast<float>(38) * digUScale;
		}
		else if(theChar == 43) // '+'
		{
			uv_x = static_cast<float>(39) * digUScale;
		}


		glm::vec2 uv_P1 = glm::vec2( uv_x,                 	1.0f );
		glm::vec2 uv_P2 = glm::vec2( uv_x + digUScale,  	1.0f );
		glm::vec2 uv_P3 = glm::vec2( uv_x + digUScale ,		0.0f );
		glm::vec2 uv_P4 = glm::vec2( uv_x,                 	0.0f );
	
		m_verticesUV.push_back(uv_P3);
		m_verticesUV.push_back(uv_P2);
		m_verticesUV.push_back(uv_P1);

		m_verticesUV.push_back(uv_P1);
		m_verticesUV.push_back(uv_P4);
		m_verticesUV.push_back(uv_P3);
	}
}
//----------------------------------------------------------------------
/*
	ONLY for updating content of text - you can NOT change the length of your text
*/	
void Text2D::UpdateText(char * text)
{
	memcpy(m_text, text, strlen(text) + 1);

	m_verticesUV.clear();
	for ( unsigned int i = 0 ; i < m_textLength ; i++ )
	{		
		// 'A' in ASCII is 65, '0' in ASCII is 48.		
		char character = m_text[i];
		int theChar = static_cast<int>(character);

		float digUScale = 1.0f /  static_cast<float>(NR_CHARS_SUPPORTED);
		float uv_x = 0.0f;
		if(theChar >= 65)
		{
			uv_x = static_cast<float>(character - 65) * digUScale;
		}
		else if(theChar >= 48 && theChar <= 57)
		{
			uv_x = static_cast<float>(26) * digUScale + static_cast<float>(character - 48) * digUScale;
		}
		else if(theChar == 32) // Spacebar
		{
			uv_x = static_cast<float>(36) * digUScale;
		}
		else if(theChar == 44 || theChar == 46)// Comma or dot
		{
			uv_x = static_cast<float>(37) * digUScale;
		}
		else if(theChar == 45) // '-'
		{
			uv_x = static_cast<float>(38) * digUScale;
		}
		else if(theChar == 43) // '+'
		{
			uv_x = static_cast<float>(39) * digUScale;
		}

		glm::vec2 uv_P1 = glm::vec2( uv_x,                 	1.0f );
		glm::vec2 uv_P2 = glm::vec2( uv_x + digUScale,  	1.0f );
		glm::vec2 uv_P3 = glm::vec2( uv_x + digUScale , 	0.0f );
		glm::vec2 uv_P4 = glm::vec2( uv_x,                 	0.0f );
	
		m_verticesUV.push_back(uv_P3);
		m_verticesUV.push_back(uv_P2);
		m_verticesUV.push_back(uv_P1);

		m_verticesUV.push_back(uv_P1);
		m_verticesUV.push_back(uv_P4);
		m_verticesUV.push_back(uv_P3);
	}

	glBindBuffer(GL_ARRAY_BUFFER, m_verticesUVVBO);
	glBufferData(GL_ARRAY_BUFFER, m_verticesUV.size() * sizeof(glm::vec2), &m_verticesUV[0], GL_STATIC_DRAW);
}
//----------------------------------------------------------------------
void Text2D::Render(const glm::mat4 &projection, const glm::mat4 &view)
{
	glUseProgram(m_programID);
	glEnable(GL_BLEND);
	glDisable(GL_DEPTH_TEST);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	glm::mat4 vp = projection * view;
	glUniformMatrix4fv(m_mvpID, 1, GL_FALSE, &vp[0][0]);

	glEnableVertexAttribArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, m_verticesVBO);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);

	glEnableVertexAttribArray(1);
	glBindBuffer(GL_ARRAY_BUFFER, m_verticesUVVBO);
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 0, (void*)0);

	glActiveTexture(GL_TEXTURE0);		
	glBindTexture(GL_TEXTURE_2D, m_text2DTextureID);
	
	// Set "myTextureSampler" sampler to use Texture Unit 0
	glUniform1i(m_textureID, 0);
	glDrawArrays(GL_TRIANGLES, 0, m_vertices.size());

	glDisableVertexAttribArray(0);
	glDisableVertexAttribArray(1);
	glDisable(GL_BLEND);
	glEnable(GL_DEPTH_TEST);
}