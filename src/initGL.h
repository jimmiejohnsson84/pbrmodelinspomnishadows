#ifndef INITGL_H
#define INITGL_H

// Forward declarations
struct GLFWwindow;

static int s_windowWidth = 1920;
static int s_windowHeight = 1080;


int InitGL(GLFWwindow** window, int windowWidth, int windowHeight, std::ofstream *outputFileDebug = NULL);

#endif