#include <iostream>
#include <vector>
using namespace std;

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include "model3d_cooktorrance.h"
#include "lightData.h"
#include "renderStruct.h"
#include "animPlayer.h"

Model3DCookTorranceAnimPlayer::Model3DCookTorranceAnimPlayer()
{
    m_animData.m_currentFrame = 0;
    m_animPlaying = false;
    m_loop = false;

    m_timeLast = -1.0f;
    m_lerpFactor = 0.0f;
}
//----------------------------------------------------------------------
Model3DCookTorranceAnimPlayer::~Model3DCookTorranceAnimPlayer()
{

}
//----------------------------------------------------------------------
void Model3DCookTorranceAnimPlayer::SetFrameData(Model3dCookTorrance* frames[MAX_NR_ANIM_FRAMES])
{
    for(int i = 0; i < MAX_NR_ANIM_FRAMES; i++)
    {
        m_animData.m_frames[i] = frames[i];
    }
}
//----------------------------------------------------------------------
void Model3DCookTorranceAnimPlayer::StartPlaying()
{
    m_animPlaying = true;
    m_timeLast = -1.0f;
    m_lerpFactor = 0.0f;
    m_animData.m_currentFrame = 0;
}
//----------------------------------------------------------------------
void Model3DCookTorranceAnimPlayer::ChangeFrameDelta(int v)
{
    m_animData.m_currentFrame += v;
    m_lerpFactor = 0.0f;
    m_timeLast = -1.0f;
}
//----------------------------------------------------------------------
void Model3DCookTorranceAnimPlayer::ChangeFrameNr(int frame)
{
    m_animData.m_currentFrame = frame;
    m_lerpFactor = 0.0f;
    m_timeLast = -1.0f;
}
//----------------------------------------------------------------------
void Model3DCookTorranceAnimPlayer::Tick(float deltaTime)
{
    if(m_animPlaying)
    {
        if(m_timeLast < 0.0f)
        {
            m_timeLast = glfwGetTime();
        }
        else
        {
            float timeDiff = glfwGetTime() - m_timeLast;

            if(timeDiff > m_frameRate)
            {
                m_timeLast = -1.0f;
                if(m_animData.m_currentFrame < m_animData.m_nrFrames - 1)
                {
                    ChangeFrameDelta(1);
                }
                else
                {
                    if(m_loop)
                    {
                        ChangeFrameNr(0);
                    }
                    else
                    {
                        m_animPlaying = false;
                    }                            
                }
            }
            else
            {
                m_lerpFactor = timeDiff / m_frameRate;
                if (m_lerpFactor > 1.0f)
                {
                    m_lerpFactor = 1.0f;
                }
                if(m_lerpFactor < 0.000001f)
                {
                    m_lerpFactor = 0.0f;
                }
            }

            if(m_animData.m_currentFrame + 1 < m_animData.m_nrFrames)
            {
                m_lerpMdl->Lerp(m_animData.m_frames[m_animData.m_currentFrame]->GetVertices(),
                                m_animData.m_frames[m_animData.m_currentFrame + 1]->GetVertices(),
                                m_lerpFactor);
            }               
        }
    }
}
//----------------------------------------------------------------------
void Model3DCookTorranceAnimPlayer::Render(const RenderStateModel3D& renderState, bool renderingReflection, 
                    const glm::mat4 &projection, const glm::mat4 &view)
{
    m_lerpMdl->Render(renderState, projection, view, m_geometryData.m_pos, 
                        m_geometryData.m_rot, m_geometryData.m_scale);
}