#ifndef GEOMETRY_DATA_H
#define GEOMETRY_DATA_H

class GeometryData
{
	public:
	GeometryData() 
	{};

	GeometryData(glm::vec3 pos, glm::vec3 rot, glm::vec3 scale) : m_pos(pos), m_rot(rot), m_scale(scale)
	{};

	glm::vec3 	m_pos;
	glm::vec3 	m_rot;
	glm::vec3	m_scale;
};


#endif