#ifndef RENDER_STRUCT
#define RENDER_STRUCT


struct OmniShadows
{
	unsigned int m_shadowMapWidth;
	unsigned int m_shadowMapHeight;

	GLuint m_depthMapFBO;
	GLuint m_depthCubemap;

	std::vector<glm::mat4> m_pointLightMVP;
};


struct RenderStateTexturedQuad3D
{
    const SceneLightData &m_sceneLightData;
    const glm::vec4 &m_clipPlane;
    bool        m_blendOn;
    bool        m_disableDepthTest;
};


struct RenderStateModel3D
{
    const SceneLightData &m_sceneLightData;
    const glm::vec4 &m_clipPlane;
    bool            m_invertLightPos;

    OmniShadows     m_omniShadows;
    bool            m_renderShadowDataPass;
};

#define USE_PBR


#endif 